;;; gnuastro.el --- An Emacs frontend for GNU Astronomy Utilities
;; Author: Markus Schaney <markus@schaney.org>

(defun astcosmiccal ()
  "An Emacs frontend for astcosmiccal, which is a part of GNU
Astronomy Utilities. This function prompts the user for a desired
redshift, then outputs various results into a split screen."
(interactive)
(let (redshift)
  (setq redshift (read-number "Enter the desired redshift (e.g. 8.2): "))
  (shell-command (concat "astcosmiccal -z " (format "%.3f redshift)))))

(defun astcosmiccal-age ()
  "This function prompts the user for a desired redshift, then inserts the
age of the universe at z (in Giga Annum) at the current point in the buffer."
(interactive)
(setq (redshift)
  (insert (shell-command-to-string (concat "astcosmiccal -g -z " (format "%.3f" redshift))))
  (backward-char)
  (insert " Ga")))

(defun astcosmiccal-absmagconv ()
  "This function prompts the user for a desired redshift, converts the redshift
to an absolute magnitude, then inserts the output to the current point in the buffer ."
(interactive)
(setq (redshift)
  (insert (shell-command-to-string (concat "astcosmiccal -a -z " (format "%.3f" redshift))))
  (backward-char)))

(defun astcosmiccal-angulardimdist ()
  "This function prompts the user for a desired redshift, calculates the angular diameter
diameter distance to the object at z, then inserts the output to the current point in the
buffer."
(interactive)
(setq (redshift)
  (insert (shell-command-to-string (concat "astcosmiccal -A -z " (format "%.3f" redshift))))
  (backward-char)
  (insert " Mpc")))

(defun astcosmiccal-distancemodulus ()
  "This function prompts the user for a desired redshift, calculates the angular diameter
diameter distance to the object at z, then inserts the distance modulus to z at the current
point in the buffer."
(interactive)
(setq (redshift)
  (insert (shell-command-to-string (concat "astcosmiccal -u -z " (format "%.3f" redshift))))
  (backward-char)))

(defun astcosmiccal-lookbacktime ()
  "This function prompts the user for a desired redshift, calculates how far back we must
look to find z, then inserts the lookback time (in Giga Annum) at the current point in the
buffer."
(interactive)
(setq (redshift)
  (insert (shell-command-to-string (concat "astcosmiccal -b -z " (format "%.3f" redshift))))
  (backward-char)
   (insert " Mpc")))
   
